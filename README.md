# publish

![python](https://img.shields.io/badge/python-3.7-blue.svg)
![build status](https://gitlab.com/thomasboni/publish/badges/master/build.svg)
![coverage report](https://gitlab.com/thomasboni/publish/badges/master/coverage.svg)


Publish pipeline jobs results in merge requests

Contents

* [Description](#description)
* [Requirements](#requirements)
* [Getting started](#getting-started)
    * [Installation](#installation)
    * [Run it !](#run-it)

## Description

This software permit to publish any data in Gitlab merge requests.

It's used mainly to publish jobs results as merge request comments. You have
to run it at the end of your jobs with jobs results (markdown formatted) as
data. It create only one note (comment) per merge request containing one
section per job. When you run a new pipeline, entire comment is replaced.

* [Changelog](CHANGELOG.md)

## Requirements

* Python >= 3.7
* Requirements in [Pipfile](Pipfile)

## Getting started

### Installation

Clone the project
```
git@gitlab.com:thomasboni/publish.git
cd publish
```

As a developer, install python environment for development:
```
pipenv install
```

As an user, install tool to use it on your system:
```
python setup.py install
```

### Run it !

```
usage: publish.py [-h] [-v] [-d] [-q] -f FILE --project-id PROJECT_ID
                  --pipeline-iid PIPELINE_IID -b BRANCH_NAME --section SECTION
                  --url URL --token TOKEN --user-id USER_ID

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Verbose mode (log INFO messages)
  -d, --debug           Debug mode (log DEBUG messages)
  -q, --quiet           Quiet mode (log only ERROR & CRITICAL messages)
  -f FILE, --file FILE  File containing data to publish
  --project-id PROJECT_ID
                        ID of project containing merge request
  --pipeline-iid PIPELINE_IID
                        IID (id scoped to project) of current pipeline
  -b BRANCH_NAME, --branch-name BRANCH_NAME
                        Name of branch which runs current pipeline
  --section SECTION     Name of section to publish
  --url URL             Url of instance to connect (including protocol)
  --token TOKEN         Private token to authenticate
  --user-id USER_ID     ID of user who publish notes
```

**Example**

Run publish in `debug` in order to push `job_result.md` content in any MR which
has branch `12-toremove-test-mr-comment-with-api` as source branch in a section
nammed `Quality check`:

```
publish -d --token <TOKEN> -f job_result.md --project-id 15311834 --section "Quality checks" --url https://gitlab.com --user-id <gitlab_user_id> --pipeline-iid 52 --branch-name "12-toremove-test-mr-comment-with-api"
```

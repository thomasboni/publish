# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.1.0] - 2019-11-17

### Added
- Permit to publish on multiple MR (with same source branch)
- Require branch name as cli argument
- Get MR from Gitlab API instead from CLI argument
- Add CLI options to set logging level
- Initial version

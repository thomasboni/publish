#!/usr/bin/env python

from glob import glob
from setuptools import setup

TEMPLATE_DIR = 'templates'

templates = glob('{}/*'.format(TEMPLATE_DIR))

setup(name='publish',
      version='0.1.0',
      description='Publish pipeline results in merge requests',
      author='Thomas Boni',
      author_email='thomasboni13@gmail.com',
      url='https://gitlab.com/thomasboni/publish',
      scripts=['cli/publish'],
      packages=['publish'],
      install_requires=['jinja2~=2.10.3',
                        'requests~=2.22.0',
                        'python-gitlab~=1.13.0'],
     )

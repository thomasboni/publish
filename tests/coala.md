## Quality check

Check results per file type:

* all: 0 MAJOR, 2 NORMAL, 0 INFO
* json: 0 MAJOR, 7 NORMAL, 0 INFO
* python: 0 MAJOR, 30 NORMAL, 0 INFO
* yaml: 53 MAJOR, 11 NORMAL, 0 INFO

More details in [full report](https://gitlab.com/tictac-toe/grid-api/-/jobs/345939968/artifacts/file/quality_check.html)
